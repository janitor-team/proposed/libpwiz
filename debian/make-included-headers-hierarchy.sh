#!/bin/sh

WORK_DIR=$PWD

DEBIAN_DIR=${WORK_DIR}/debian

INCLUDES_DIR=${DEBIAN_DIR}/includes/proteowizard

rm -rf ${INCLUDES_DIR}

for file in $(find . -type f | grep ".*\.h[p]\{0,2\}$" | sed 's|^./||')
do 
	# except files in debian/, .pc/ and doc/ !!!
	echo ${file} | grep ^debian
	debianDir=$?

	echo ${file} | grep ^doc
	docDir=$?

	echo ${file} | grep ^\.pc
	dotPcDir=$?

	if [ "${debianDir}" != 0 ] && [ "${docDir}" != 0 ] && [ "${dotPcDir}" != 0 ] 
	then
		baseName=$(basename ${file})
		# echo "baseName: ${baseName}"

		dirName=$(dirname ${file})
		# echo "dirName: ${dirName}"

		mkdir -p ${INCLUDES_DIR}/${dirName}
		cp ${file} ${INCLUDES_DIR}/${dirName}
	fi
done

# Note that there are also *.inl files.

mkdir -p ${DEBIAN_DIR}/includes/proteowizard/pwiz/data/common && \
cp pwiz/data/common/cv.inl \
${DEBIAN_DIR}/includes/proteowizard/pwiz/data/common/cv.inl
