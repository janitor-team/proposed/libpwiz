<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN">
<refentry>
  <refentryinfo>
    <date>05 december 2013</date>
  </refentryinfo>
  <refmeta>
    <refentrytitle>
      <application>MSCONVERT</application>
    </refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo>msconvert</refmiscinfo>
  </refmeta>
  <refnamediv>
    <refname>
      <application>msconvert</application>
    </refname>
    <refpurpose>
      Convert mass spectrometry data file formats.
    </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>msconvert</command>
      <arg><option><replaceable class="parameter">options</replaceable></option></arg>
      <arg><option><replaceable class="parameter">filemasks</replaceable></option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>
    <para>
      This manual page documents briefly
      the <command>msconvert</command> software shipped within the
      <filename>libpwiz-tools</filename> package. This program allows
      one to convert mass spectrometry data files from one format to
      another. Since this version is built on the Free Software Debian
      platform, only conversions involving Free Software can be
      performed [that is, *not* involving proprietary Microsoft
      Windows-based dynamic linking libraries (dlls)].
    </para>
  </refsect1>
  <refsect1>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
	<term>-v | --verbose</term>
	<listitem>
	  <para>
	    Display detailed processing progress information.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--help</term>
	<listitem>
	  <para>
	    Show this message, with extra detail on filter options.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>-f | --filelist <replaceable class="parameter">filename</replaceable></term>
	<listitem>
	  <para>
	    Uses the contents of <replaceable class="parameter">filename</replaceable> which lists filenames.
	  </para>
	</listitem>      
      </varlistentry>

      <varlistentry>
	<term>-o | --outdir <replaceable class="parameter">dir</replaceable></term>
	<listitem>
	  <para>
	    Set the output directory ('-' for stdout) to <replaceable class="parameter">dir</replaceable>. By default, the output directory is '.' (that is, the current working directory).
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term>-c | --config <replaceable class="parameter">filename</replaceable></term>
	<listitem>
	  <para>
	    Set the configuration file to <replaceable class="parameter">filename</replaceable>.
	  </para>
	</listitem>
      </varlistentry>
      
      <varlistentry>
	<term>--outfile <replaceable class="parameter">filename</replaceable></term>
	<listitem>
	  <para>
	    Override the name of the output file.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>-e | --ext <replaceable class="parameter">ext</replaceable></term>
	<listitem>
	  <para>
	    Set the extension of the output files
	    to <replaceable class="parameter">ext</replaceable>. Can
	    be mzML or mzXML or mgf or txt.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--mzML</term>
	<listitem>
	  <para>
	    Write <filename>mzML</filename> format (default).
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--mzXML</term>
	<listitem>
	  <para>
	    Write <filename>mzXML</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--mgf</term>
	<listitem>
	  <para>
	    Write <filename>mgf</filename> Mascot generic format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--text</term>
	<listitem>
	  <para>
	    Write ProteoWizard internal <filename>text</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--ms1</term>
	<listitem>
	  <para>
	    Write <filename>MS1</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--cms1</term>
	<listitem>
	  <para>
	    Write <filename>CMS1</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--ms2</term>
	<listitem>
	  <para>
	    Write <filename>MS2</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--cms2</term>
	<listitem>
	  <para>
	    Write <filename>CMS2</filename> format.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--64</term>
	<listitem>
	  <para>
	    Set default binary encoding to 64-bit precision (default).
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--32</term>
	<listitem>
	  <para>
	    Set default binary encoding to 32-bit precision.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--mz64</term>
	<listitem>
	  <para>
	    Encode m/z values in 64-bit precision (default).
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--mz32</term>
	<listitem>
	  <para>
	    Encode m/z values in 32-bit precision.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--inten64</term>
	<listitem>
	  <para>
	    Encode intensity values in 64-bit precision.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--inten32</term>
	<listitem>
	  <para>
	    Encode intensity values in 32-bit precision (default).
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--noindex</term>
	<listitem>
	  <para>
	    Do not write index.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>-i | --contactInfo <replaceable class="parameter">filename</replaceable></term>
	<listitem>
	  <para>
	    Use <replaceable class="parameter">filename</replaceable> for contact info.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>-z | --zlib</term>
	<listitem>
	  <para>
	    Use zlib compression for binary data.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>-g | --gzip</term>
	<listitem>
	  <para>
	    gzip entire output file (adds .gz to filename).
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--filter <replaceable class="parameter">arg</replaceable></term>
	<listitem>
	  <para>
	    Add a spectrum list filter.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--merge</term>
	<listitem>
	  <para>
	    Create a single output file from multiple input files by merging file-level metadata and concatenating spectrum lists.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--simAsSpectra</term>
	<listitem>
	  <para>
	    Write selected ion monitoring as spectra, not chromatograms. 
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>--srmAsSpectra</term>
	<listitem>
	  <para>
	    Write selected reaction monitoring as spectra, not chromatograms. 
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>EXAMPLES</title>
    <para>
      Convert data.RAW to data.mzXML
    </para>
    <para>
      <command>msconvert data.RAW --mzXML</command>
    </para>

    <para>
      Put output file in my_output_dir
    </para>
    <para>
      <command>msconvert data.RAW -o my_output_dir</command>
    </para>

    <para>
      Extract scan indices 5...10 and 20...25
    </para>
    <para>
      <command>msconvert data.RAW --filter "index [5,10] [20,25]"</command>
    </para>

    <para>
      Extract MS1 scans only
    </para>
    <para>
      <command>msconvert data.RAW --filter "msLevel 1"</command>
    </para>

    <para>
      Extract MSn scans for n>1
    </para>
    <para>
      <command>msconvert data.RAW --filter "msLevel 2-"</command>
    </para>

    <para>
      Use a configuration file
    </para>
    <para>
      <command>msconvert data.RAW -c config.txt</command>
    </para>
  </refsect1>

  <refsect1>
    <title>RETURN VALUE</title>
    <para>
      The number of failed files.
    </para>
  </refsect1>

  <refsect1>
    <title>AUTHOR</title>
    <para>
      <author>
	<firstname>Filippo</firstname>
	<surname>Rusconi &lt;lopippo@debian.org&gt;</surname>
	<authorblurb>
	  <para>
	    This manual page was written by Filippo Rusconi
	    &lt;lopippo@debian.org&gt; (initial writing 05 december
	    2013). Permission is granted to copy, distribute and/or
	    modify this document under the terms of the GNU General
	    Public License, Version 3, published by the Free Software
	    Foundation.
	  </para>
	  
	  <para>
	    On a Debian system the complete text of the GNU General
	    Public License version 3 can be found in the file
	    `<filename>/usr/share/common-licenses/GPL\-3</filename>'.
	  </para>

	</authorblurb>
      </author>
    </para>
  </refsect1>
</refentry>









