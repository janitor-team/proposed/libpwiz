Source: libpwiz
Priority: optional
Maintainer: The Debichem Group <debichem-devel@lists.alioth.debian.org>
Uploaders: Filippo Rusconi <lopippo@debian.org>
Build-Depends: debhelper-compat (= 12),
               dpkg-dev (>= 1.16.1~),
               chrpath (>= 0.13),
               libboost-tools-dev,
               doxygen (>= 1.8.1.2),
               docbook-to-man,
               libboost-program-options-dev (>= 1.62.0),
               libboost-date-time-dev (>= 1.62.0),
               libboost-iostreams-dev (>= 1.62.0),
               libboost-regex-dev (>= 1.62.0),
               libboost-filesystem-dev (>= 1.62.0),
               libboost-thread-dev (>= 1.62.0),
               libboost-system-dev (>= 1.62.0),
               libboost-chrono-dev (>= 1.62.0),
               libboost-dev (>= 1.62.0),
               libfftw3-dev (>= 3.3.2),
               libgd-dev,
               libjs-jquery,
               libeigen3-dev
Standards-Version: 4.5.0
Section: libs
Homepage: http://proteowizard.sourceforge.net/downloads.shtml
Vcs-Browser: https://salsa.debian.org/debichem-team/libpwiz
Vcs-Git: https://salsa.debian.org/debichem-team/libpwiz.git

Package: libpwiz-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libpwiz3 (= ${binary:Version})
Suggests: libpwiz-doc
Description: library to perform proteomics data analyses (devel files)
 The libpwiz library from the ProteoWizard project provides a modular
 and extensible set of open-source, cross-platform tools and
 libraries. The tools perform proteomics data analyses; the libraries
 enable rapid tool creation by providing a robust, pluggable
 development framework that simplifies and unifies data file access,
 and performs standard chemistry and LCMS dataset computations.
 .
 The primary goal of ProteoWizard is to eliminate the existing
 barriers to proteomic software development so that researchers can
 focus on the development of new analytic approaches, rather than
 having to dedicate significant resources to mundane (if important)
 tasks, like reading data files.
 .
 This package ships the library development files.

Package: libpwiz3
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libpwiz-doc
Description: library to perform proteomics data analyses (runtime)
 The libpwiz library from the ProteoWizard project provides a modular
 and extensible set of open-source, cross-platform tools and
 libraries. The tools perform proteomics data analyses; the libraries
 enable rapid tool creation by providing a robust, pluggable
 development framework that simplifies and unifies data file access,
 and performs standard chemistry and LCMS dataset computations.
 .
 The primary goal of ProteoWizard is to eliminate the existing
 barriers to proteomic software development so that researchers can
 focus on the development of new analytic approaches, rather than
 having to dedicate significant resources to mundane (if important)
 tasks, like reading data files.

Package: libpwiz-tools
Section: science
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libpwiz3 (= ${binary:Version})
Suggests: libpwiz-doc
Description: ProteoWizard command line tools
 The libpwiz library from the ProteoWizard project provides a modular
 and extensible set of open-source, cross-platform tools and
 libraries. The tools perform proteomics data analyses; the libraries
 enable rapid tool creation by providing a robust, pluggable
 development framework that simplifies and unifies data file access,
 and performs standard chemistry and LCMS dataset computations.
 .
 The primary goal of ProteoWizard is to eliminate the existing
 barriers to proteomic software development so that researchers can
 focus on the development of new analytic approaches, rather than
 having to dedicate significant resources to mundane (if important)
 tasks, like reading data files.
 .
 This package ships command line tools that include _idconvert_
 (conversion of MS identifications) and _msconvert_ (conversion of MS
 raw data files from/to any supported format).

Package: libpwiz-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         libjs-jquery
Description: set of programs to perform proteomics data analyses (doc)
 The libpwiz library from the ProteoWizard project provides a modular
 and extensible set of open-source, cross-platform tools and
 libraries. The tools perform proteomics data analyses; the libraries
 enable rapid tool creation by providing a robust, pluggable
 development framework that simplifies and unifies data file access,
 and performs standard chemistry and LCMS dataset computations.
 .
 The primary goal of ProteoWizard is to eliminate the existing
 barriers to proteomic software development so that researchers can
 focus on the development of new analytic approaches, rather than
 having to dedicate significant resources to mundane (if important)
 tasks, like reading data files.
 .
 This package ships the documentation to the proteowizard software
 along with example programs (source code and binaries).

